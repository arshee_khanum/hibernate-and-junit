package com.journaldev.main;


import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.journaldev.dao.PersonDAO;
import com.journaldev.model.Person;
import com.journaldev.main.TestClass;

//import org.junit.*;


public class SpringHibernateMain {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		
		PersonDAO personDAO = context.getBean(PersonDAO.class);
		
		Person person = new Person();
		person.setName("Peushhhhhh"); person.setCountry("India");
		
		personDAO.save(person);
		
		System.out.println("Person::"+person);
		
		List<Person> list = personDAO.list();
		
		for(Person p : list){
			System.out.println("Person List::"+p);
		}
		Person personForTesting = new Person() ; 
		
		for(Person p : list){
			if(p.getName().matches("Shashank")){
				personForTesting = p;
			}
		}
		//Person personForTesting  = personDAO.getPerson("Arshee");
		//TestClass.testMethod(personForTesting);
		
		context.close();
		
	}
	
	//@Test
	//public static void testMethod(Person person) {
		//System.out.println("Inside TestMethod !!!!");
		//Assert.assertEquals(1, person.getId());
	//}

}
